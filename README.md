## Video Streaming Application

This repo contains the work done by Konstantin Bogdanoski and Natasha Stojanova for the project VSA.

You can check out the [wiki](https://github.com/Konstantin-Bogdanoski/VSA/wiki)

##### Проектот не е наменет за дистрибуирање. Сите права се задржани.
> ##### The project is not meant for redistribution. All rights reserved.
